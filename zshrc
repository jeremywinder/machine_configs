# Set the directory we want to store zinit and plugins
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"

# Download Zinit, if it's not there yet
if [ ! -d "$ZINIT_HOME" ]; then
   mkdir -p "$(dirname $ZINIT_HOME)"
   git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
fi

# Install Dependancies
if [[ "$(uname)" == "Darwin" ]] && [ -x "$(command -v brew)" ]; then
   # Install dependancies on macOS using HomeBrew
   macos_dependancies=(fzf zoxide starship)
   for PACKAGE in $macos_dependancies; do
      if [ ! -x "$(command -v $PACKAGE)" ]; then
         brew install $PACKAGE
      fi
   done
else
   print -P "%F{red}WARN:%f OS not supported by dependancy checks"
fi

# Source/Load zinit
source "${ZINIT_HOME}/zinit.zsh"

# Initialize starship
PROMPT="$fg[cyan]%}$USER@%{$fg[blue]%}%m ${PROMPT}"
eval "$(starship init zsh)"

# Load completions
autoload -Uz compinit && compinit

# Setup OpenShift Local
if [ -x "$(command -v crc)" ]; then
   source <(crc completion zsh)
   eval $(crc oc-env)
   source <(oc completion zsh)
fi

# Add in zsh plugins
# NOTE: Aloxaf/fzf-tab has to be loaded after completions
zinit light zdharma-continuum/fast-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions
zinit light Aloxaf/fzf-tab

# History
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

# Aliases
alias ls='ls --color'

# Shell integrations
eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"
